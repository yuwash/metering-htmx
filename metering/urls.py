from django.urls import path

from . import views

urlpatterns = [
    path('', views.MainView.as_view(), name='main'),
    path(
        'reading/<int:pk>',
        views.ReadingEditView.as_view(),
        name='reading',
    ),
    path(
        'reading/<int:pk>/edit',
        views.ReadingEditView.as_view(do_edit=True),
        name='reading-edit',
    ),
]