from django.contrib import admin
from . import models

@admin.register(models.Series, models.Reading)
class ReadingAdmin(admin.ModelAdmin):
    pass
