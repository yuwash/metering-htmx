from django.db import models

class Series(models.Model):
    label = models.CharField(max_length=100)


class Reading(models.Model):
    series = models.ForeignKey(Series, on_delete=models.CASCADE)
    value = models.IntegerField()
    read_at = models.DateTimeField()
