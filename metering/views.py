import datetime
import enum
import functools
import typing
from django import urls
from django.shortcuts import render
from django.utils import html
from django.views import generic
import django_tables2 as tables
from . import models


class Action(enum.Enum):
    edit = enum.auto()
    cancel = enum.auto()


DEFAULT_ACTIONS = (Action.edit,)
URL_FOR_ACTION = {
    Action.edit: "metering:reading-edit",
    Action.cancel: "metering:reading",
}


def render_actions(record: models.Reading, value: typing.Optional[typing.Tuple[Action]] = None):
    if not value:
        value = DEFAULT_ACTIONS
    return html.format_html_join(
        "",
        '<button class="pure-button" hx-get="{}">{}</button>',
        (
            (
                urls.reverse(URL_FOR_ACTION[action], kwargs={"pk": record.id}),
                action.name,
            )
            for action in value
        ),
    )


class ReadingTable(tables.Table):
    actions = tables.Column(
        empty_values=(),  # To force render_foo to be called for None
        exclude_from_export=True
    )

    def render_actions(self, record, value):
        return render_actions(record=record, value=value)

    class Meta:
        model = models.Reading
        attrs = {
            "class": "pure-table",
            "tbody": {
                "hx-target": "closest tr",
                "hx-swap": "outerHTML",
            },
        }
    
    def render_series(self, value: models.Series):
        return value.label


class MainView(tables.SingleTableView):
    model = models.Reading
    table_class = ReadingTable
    template_name = 'main.html'
    extra_context = {"title": "Metering"}

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        return dict(context, **self.extra_context)


class Reading(typing.NamedTuple):
    id: int
    series: str
    value: int
    read_at: datetime.datetime

    @classmethod
    def from_model(cls, reading: models.Reading):
        return cls(
            id=reading.id,
            series=reading.series.label,
            value=reading.value,
            read_at=reading.read_at,
        )

    def render_row(self):
        return self + (render_actions(self),)

    def render_edit_row(self):
        return (
            self.id,
            self.series,
            html.format_html(
                '<input name="value" type="number" value="{}" hx-patch="{}">',
                self.value,
                urls.reverse(
                    'metering:reading-edit', kwargs={'pk': self.id}
                ),
            ),
            self.read_at,
            render_actions(self, (Action.cancel,)),
        )


class ReadingEditView(generic.TemplateView):
    template_name = 'reading-edit.html'
    do_edit = False

    @functools.cached_property
    def obj(self):
        return models.Reading.objects.get(id=self.request.resolver_match.kwargs['pk'])

    def get_context_data(self, **kwargs):
        reading_tuple = Reading.from_model(self.obj)
        row = {
            'GET': reading_tuple.render_edit_row(),
            'PATCH': reading_tuple.render_row(),
        }[self.request.method] if self.do_edit else reading_tuple.render_row()
        context = dict(
            super().get_context_data(**kwargs), reading=self.obj, row=row
        )
        return context

    def patch(self, request, **kwargs):
        # FIXME Somehow POST is always empty here
        if value := request.POST.get('value'):
            self.obj.value = value
            self.obj.save()
        return self.get(request, **kwargs)
